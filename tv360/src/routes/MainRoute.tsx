import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { RouteName } from '../values/RouteName';
import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen';

const Stack = createNativeStackNavigator();

const routes = [
  {
    name: RouteName.HOME_SCREEN,
    component: HomeScreen
  },
  {
    name: RouteName.DETAIL_SCREEN,
    component: DetailScreen
  },
]

export const MainRoute = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {
        routes.map((r, i) => <Stack.Screen {...r} key={i} />)
      }
    </Stack.Navigator>
  )
}