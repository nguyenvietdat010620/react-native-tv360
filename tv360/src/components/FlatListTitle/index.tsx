import React from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  SectionList,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import stringPro from '../../utils/StringProcess';
import { Navigator } from '../../utils/Navigator';
import { RouteName } from '../../values/RouteName';
const FlatListTitle = ({
  listData
}: {
  listData: any
}) => {

  const renderItem = ({ item, index }: { item: any, index: number }) => {
    return (
      <View>
        <Text style={{ color: 'white', fontSize: 18, fontWeight: '700', marginVertical: 10 }}>{item.name}</Text>
        <FlatList
          keyExtractor={item => item.id}
          showsHorizontalScrollIndicator={false}
          horizontal
          data={item.content}
          renderItem={renderItemImage}
        />
      </View>
    )
  }

  const renderItemImage = ({ item, index }: { item: any, index: number }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          console.log('oke');
          Navigator.navigate(RouteName.DETAIL_SCREEN)
        }}
        style={{ marginRight: 8 }}>
        <Image style={{ height: 100, width: 148, borderRadius: 3 }} source={{ uri: stringPro(item.coverImage) }}></Image>
      </TouchableOpacity>
    )
  }

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      style={{ marginHorizontal: 10 }}
      data={listData}
      renderItem={renderItem}
    />
  )
}

export default FlatListTitle