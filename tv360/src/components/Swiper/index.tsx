import React from 'react';
import {
  Dimensions,
  Image,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Swiper from 'react-native-swiper'
import stringPro from '../../utils/StringProcess';

const SwiperComponent = ({
  listImage
}: {
  listImage: any
}) => {
  return (
    <View style={{ height:200, marginVertical:16 }}>
      <Swiper
        loop={true}
        autoplay={true}
        pagingEnabled={true}
        showsPagination={false}
        autoplayTimeout={5}
        style={{ flexDirection: 'row', height: 200 ,alignItems: 'center' }}>
        {
          listImage.map((item: any, index: number) => {
            return (
              <View
                key={index}
                style={{
                  // width: Dimensions.get('window').width,
                  height:200
                }}>
                <Image style={{ flex:1 }} resizeMode='contain' source={{ uri: stringPro(item.coverImage) }}></Image>
              </View>
            );
          })
        }
      </Swiper>
    </View>
  );
};

export default SwiperComponent