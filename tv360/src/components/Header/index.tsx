import React from 'react';
import {
  Image,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import images from '../../config/images';
import { getStatusBarHeight } from 'react-native-iphone-x-helper'

const Header = () => {

  return (
    <View style={{ height:72, paddingTop: getStatusBarHeight() +10, marginHorizontal:10 }}>
      <StatusBar backgroundColor={'#fff'}
      barStyle={'light-content'}></StatusBar>
      <View
        style={{ flex: 1, height: 72, flexDirection: 'row', alignItems: 'center',backgroundColor:'black'}}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center', flex:1 }}>
          <Image style={{ height: 14, width: 18 }} source={images.menu}></Image>
          <Image style={{ height: 21.84, width: 58.42, marginLeft:16 }} source={images.logov2}></Image>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', flex:1, justifyContent:'space-between' }}>
          <Image style={{ height: 17.98, width: 18 }} source={images.search_icon}></Image>
          <Image style={{ height: 18, width: 16, marginLeft:16 }} source={images.notification}></Image>
          <TouchableOpacity style={{backgroundColor:'#ED2C25', height:32,
           width:95, justifyContent:'center', alignItems:'center', borderRadius:8
           }}>
            <Text style={{color:'white'}}>Mua Gói</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}
export default Header