import React from 'react';

import { StackActions } from '@react-navigation/native';

const navigationReference = React.createRef<any>();

const navigate = (name: string, params?: object) => navigationReference.current?.navigate(name, params);

const resetAndPush = (name: string, params?: object) => navigationReference.current?.reset({
  index: 0,
  routes: [{ name, params }]
})

const push = (name: string, params?: object) => {
  const popAction = StackActions.push(name, params);
  navigationReference.current?.dispatch(popAction);
};

const popScreen = (n: number) => {
  const popAction = StackActions.pop(n);
  navigationReference.current?.dispatch(popAction);
};

const goBack = () => navigationReference.current.goBack();

export const Navigator = {
  navigationReference,
  navigate,
  resetAndPush,
  popScreen,
  goBack,
  push
}