import axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  ScrollView,
  StatusBar,
  View,
} from 'react-native';
import Header from '../../components/Header';
import _ from 'lodash'
import SwiperComponent from '../../components/Swiper';
import FlatListTitle from '../../components/FlatListTitle';
const HomeScreen = () => {
  const [dataBanner, setdataBanner] = useState([])
  const [listData, setlistData] = useState<any>([])
  const header: any = {
    'Content-Type': 'application/json',
    'User-Agent': 'PostmanRuntime/7.30.1',
    'Accept': '*/*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'deviceid': 'wap_lx4230g34f420ns43laskl342sl76',
    'devicetype': 'Android',
    'lang': 'vi',
    'osapptype': 'WAP',
    'osappversion': '0.1.0',
    'sessionid': '635f4d7a-45f0-4c71-9943-865db7c4252a',
    'zoneid': '1',
    // '#Authorization':'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjkiLCJ1c2VySWQiOjEyOSwicHJvZmlsZUlkIjo5OSwiZHZpIjoxMTgzNCwiY29udGVudEZpbHRlciI6IjEzIiwiZ25hbWUiOiIiLCJpYXQiOjE2MDQwNTcwMTUsImV4cCI6MTYwNDA1NzMxNX0.-ZJNkJyTtwkmHlyU-lIro5jWjWbzEazEkQZMlwcnobNEL5vjXp6qRstj5wv4ci_BORcM5izLyKv4z5LoRm0MHg'
  }

  const getData = async () => {
    await axios.get('https://api.tv360.vn/public/v1/composite/get-home?offset=0&limit=10', { headers: header })
      .then((res: any) => {
        const data = res.data.data
        const index = _.findIndex(data, (e: any) => {
          return e.type === "BANNER"
        }, 0)
        setdataBanner(data[index].content)
        const arr_filter = _.filter(data, function (o) {
          return o.content && o.type != "BANNER"
        })
        setlistData(arr_filter)
      })
      .catch((err: any) => {
        console.log(err);
      })
  }
  useEffect(() => {
    getData()
  })

  return (
    <View
      style={{ backgroundColor: 'black', flex: 1 }}
    >
      <Header></Header>
      <ScrollView>
        <SwiperComponent listImage={dataBanner}></SwiperComponent>
        <FlatListTitle listData={listData}></FlatListTitle>
      </ScrollView>
    </View>
  )
}
export default HomeScreen