const images = {
  logo: require('../assets/images/logo360.png'),
  logov2: require('../assets/images/logo.png'),
  menu: require('../assets/images/menu.png'),
  notification: require('../assets/images/notification.png'),
  search_icon: require('../assets/images/search.png')
}
export default images