module.exports = {
  presets: [
    'module:metro-react-native-babel-preset'
  ],
  env: {
    production: {
      plugins: ['react-native-paper/babel'],
    },
  },
  plugins: [
    'react-native-reanimated/plugin',
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: [
          '.ios.ts',
          '.android.ts',
          '.ts',
          '.ios.tsx',
          '.android.tsx',
          '.tsx',
          '.jsx',
          '.js',
          '.json',
        ],
        alias: {
          "@app": "./src",
          "@app/apis": "./src/apis",
          "@app/components": "./src/components",
          "@app/assets": "./src/assets",
          "@app/locales": "./src/locales",
          "@app/redux": "./src/redux",
          "@app/utils": "./src/utils",
          "@app/values": "./src/values",
          "@app/screens": "./src/screens",
          "@app/types": "./src/types",
          "@app/routes": "./src/screens"
        },
      },
    ],
  ],
};